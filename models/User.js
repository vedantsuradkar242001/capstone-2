
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	buyed : [
		{
			productId : {
				type : String,
				required : [true, "Product ID is required"]
			},
			buyedOn : {
				type : Date,
				default : new Date()
			},
			status : {
				type : String,
				default : "buyed"
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);
