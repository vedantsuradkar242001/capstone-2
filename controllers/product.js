const Product = require("../models/Product");


module.exports.addProduct = (data) => {

	if (data.isAdmin) {
	let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});

		return newProduct.save().then((product, error) => {

			if (error) {

				return false;

			} else {

				return true;

			};

		});

		} else {
		return false;
	};
	

};



module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result
	})
}


module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

module.exports.updateProduct = (reqParams, reqBody) => {
	// Specify the fields/properties of the document to be updated
	let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		
		if(error){
			return false;
		}
	
		else{
			return true;
		}
	})
}

module.exports.archiveProduct = (reqParams, reqBody) => {

	let updateActiveField = {
		isActive : reqBody.isActive
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if (error) {

			return false;

		} else {

			return true;

		}

	});
};

